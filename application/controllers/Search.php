<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('Search');

    }
//a user will be able to enter in a string and the result should be that a matching string should appear
    public function doSearch()
    {
        //load model
        $this->load->model('Messages_model');
        //load function and retrieve GET method
        $data['posts'] = $this->Messages_model->searchMessages($_GET['search']);
            $this->load->view('ViewMessages', $data);




    }
}