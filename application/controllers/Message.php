 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');

    }
    public function index()
    {
    	//if a session has not been set (FALSE) the user should be redirected to login page
      		if ($this->session->userdata('username') == FALSE) {
                redirect(site_url('/user/login'));
			}
            else {
                   $this->load->view('Post');
            }

    }
    //if the user is logged in, they should be able to post a message and after it should redirect the user to their
    //profile to view the message that has just been posted
    public function doPost() {

        $this->load->model('Messages_model');

        $postedBy = $this->session->userdata('username');
	    $string = $this->input->post('post');
        
        $this->Messages_model->insertMessage($postedBy, $string); 
            redirect(site_url('/user/view/'.$postedBy));


  
    }
}

