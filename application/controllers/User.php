<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('security');
    }

    public function index()
    {

    }

//loads messages_model and run the getMessagesByPoster() passing the specified name
    // and display output in the viewMessages view
    public function view($name = null)
    {
        $this->load->model('Messages_model');
        $data['posts'] = $this->Messages_model->getMessagesByPoster($name);
        $data['user'] = $this->session->userdata('username');
        $data['name'] = xss_clean($name);

        $this->load->model('Users_model');
        $data['follow'] = array($this->Users_model->isFollowing($this->session->userdata('username'), $name), true); 
        $this->load->view('ViewMessages', $data); //passing data into view ($data contains username of our user)
    }

    public function login()
    {
    
        $this->load->view('Login');
    }

    // checking if the user is using the right credential or not by comparing the password (using sha1 encryption before comparing)
    //if login credential is right, create a session and load messages of that particular user
    public function doLogin()
    {
        $this->load->library('session');
        $this->load->model('Users_model');
        $username = $_POST['username'];
        $password = do_hash(($this->input->post('password')), 'sha1');
        $loginUser = $this->Users_model->checkLogin($username, $password);

        echo $loginUser;
        if ($loginUser) {

            $sess_Login = array('username' => $username);
            $this->session->set_userdata($sess_Login);
            $user = $this->session->userdata('username');

            $this->load->model('Messages_model');
            $data['posts'] = $this->Messages_model->getMessagesByPoster($user);

            $this->load->view('ViewMessages', $data);
            header('Location: '.site_url().'/user/view/'.$user);

        } else {
            redirect(site_url('/user/login')); //if the login is wrong, redirect to login page


        }
    }

    //log the userout and destroy the session that was stored  and redirect user to the login page
    public function logout()
    {
        //destroy session when user is logged out and redirect to login form
        $this->session->sess_destroy();
        echo '<script>alert("Bye!");</script>';
        redirect(site_url('/user/login/'));

    }

//display messages that the user is following, ordered by date 
    public function feed($name = NULL)
    {
        $name = $this->session->userdata('username');
        $this->load->model('Messages_model');
        $data['posts'] = $this->Messages_model->getFollowedMessages($name);
        $this->load->view('ViewMessages', $data);

    }

    public function follow($followed = NULL)
    {
        //Loads the Users_model, and invokes the follow() function to add the entry into the database table.
        // Should redirect back to the /user/view/{followed} page when done
        $this->load->model('Users_model');
        $this->Users_model->follow($followed);

        redirect(site_url('/user/view/' . $followed));


    }

}