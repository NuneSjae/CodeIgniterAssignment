<!DOCTYPE html>
<html>
<head>

 <!--   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css"> -->
   <style>
input[type="password"], input[type="text"] {
  background: url('') center left no-repeat, linear-gradient(top, #d6d7d7, #929090);
  border: 1px solid #929090;
  border-radius: 2px;
  box-shadow: 0 1px #fff;
  box-sizing: border-box;
  color: #929090;
  height: 35px;
  margin: 10px 0 0 15px;
  padding-left: 37px;
  transition: box-shadow 0.3s;
  width: 250px;
}
input[type="password"]:focus, input[type="text"]:focus {
  box-shadow: 0 0 4px 1px rgba(55, 166, 155, 0.3);
}

.button {
    background-color: white;
    color: black;
    border: 2px solid #e7e7e7;
    color: black;
    padding: 12px 28px;
    margin: 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;   
}

.button:hover {
  background-color: #e7e7e7;
}

.shadow {
  background: #000;
  border-radius: 12px 12px 4px 4px;
  box-shadow: 0 0 20px 10px #000;
  height: 12px;
  margin: 30px auto;
  opacity: 0.2;
  width: 270px;
}
.Outtercenter {
	margin: auto;
  max-width: 30%;
  margin-top: 30px;
  text-align: center;
  padding: 10px;
  -moz-border-radius:30px;
  -webkit-border-radius:30px;
  border-radius:5px;
  border: #solid 3px white;
  background-color: rgba(255,255,255,0.5);



  }
.center {
	margin: auto;
  width: 50%;

}
h1 {
  text-align: center;
  color: #797979;
}
label {
  font-size: 18px;
  color: #797979;

}
body {
  background-image: url('https://upload.wikimedia.org/wikipedia/commons/d/d0/Elegant_Background-3.jpg');
}
div.login {
    margin: auto;
    width: 50%;
}

input[type="submit"]:active {
  top:3px;
  box-shadow: inset 0px 1px 0px #2ab7ec, 0px 2px 0px 0px #31524d, 0px 5px 3px #999;
}
   </style>
    <title>Welcome to Twittie</title>
</head>
<body>
	<div class="Outtercenter">
<h1>Please login...</h1>
<!-- Login form, consisting of an input box for username and password -->
<form class="center" action="https://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/user/dologin" method="POST">
    <label>Username: </label></label><input type="text" name="username" placeholder="Username" required/><br>
    <label>Password: </label></label><input type="password" name="password" placeholder="Password" required/><br>
    <center><button class="button button">Login!</button></center>
</form>
</div>

</body>
</html>
    

