<html>
<head>
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            .Outtercenter {
                margin: auto;
                margin-top: 30px;
                width: 59%;
                height: 90%;
                text-align: center;
                padding: 10px;
                -moz-border-radius: 30px;
                -webkit-border-radius: 30px;
                border-radius: 10px;
                background-color: rgba(255, 255, 255, 0.5);
            }
            .innerCenter {
                margin: auto;
                margin-top: 30px;
                width: 53%;
                text-align: center;
                padding: 10px;
            }

            h1 {
                text-align: center;
                color: #797979;
            }

            input {
                font-size: 16px;
            }

            body {
                background-image: url('https://upload.wikimedia.org/wikipedia/commons/d/d0/Elegant_Background-3.jpg');
                background-attachment: fixed;
            }

            table {
                border-collapse: collapse;
                text-align: center;
                margin: auto;
                box-shadow: 10px 7px 6px #929090;
                width: 75%;
            }

            table, th {
                border: 1px solid black;
                margin-top: 40px;
            }

            input[type="submit"]:active {
                top: 3px;
                box-shadow: inset 0px 1px 0px #2ab7ec, 0px 2px 0px 0px #31524d, 0px 5px 3px #999;
            }

            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #333;
            }

            li {
                float: left;
            }

            li a {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            li a:hover {
                background-color: #963D3D;
            }

            .button {
                background-color: white;
                color: black;
                border: 2px solid #e7e7e7;
                color: black;
                padding: 12px 28px;
                margin: 15px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
            }

            .button:hover {
                background-color: #e7e7e7;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                bottom: -50px;
                width: 100%;
                height: 39px;
                background-color: #333;
                color: #ffffff;
                text-align: center;
                font-size: 15px;
            }


        </style>
    </head>
<body>
<div class="Outtercenter">
<!--navigation bar -->

    <ul>
        <li>
            <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/user/feed">MY FEED</a>
        </li>
        <li>
            <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/search">SEARCH</a>
        </li>
        <li>
            <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/message">POST A
                MESSAGE</a>
        </li>
        <li style="float: right;" method="POST" name="logout">
            <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/user/logout">LOGOUT</a>
        </li>
    </ul>

<div>
    <!--displaying name, posts and date of posts from newest to oldest in a table, using a for eachloop to go through the database and 
        putting the names in a table below -->
            <table>
                <th>User</th>
                <th>Message</th>
                <th>Date</th>
                <?php
                foreach ($posts as $row) {
                    ?>
                    <tr>
                        <td><b><?php echo $row->user_username; ?></b></td>
                        <td><?php echo $row->text; ?></td>
                    <td><i><?php echo $row->posted_at; ?></i></td>
                 </tr>             
                <?php }
                if($this->session->userdata('username')) {
                    if (isset($follow) && ($follow[1] == true) && ($follow[0] != true)) {
                        echo '<a href="' . site_url() . '/user/follow/' . $name . '">Follow Me</a>';
                    } else if (isset($follow) && ($follow[1] == true) && isset($follow[0]) && ($follow[0] == true)) {
                        echo '<a dsabled>You are following ' . $name . '</a>';
                    }
                }
               ?>
            </table>  
    </div>
</div>
<div class="footer"><p>(C) Nun Srijae | 2017 </p></div>
</body>
</html>