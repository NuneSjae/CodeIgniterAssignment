<!DOCTYPE html>
<html>
<head>
    <!--   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css"> -->
    <style>
        input[type="text"] {
            background: url('http://i.minus.com/ibhqW9Buanohx2.png') center left no-repeat, linear-gradient(top, #d6d7d7, #929090);
            border: 2px solid #929090;
            border-radius: 4px;
            box-shadow: 0 4px #E7E7E7;
            box-sizing: border-box;
            color: #929090;
            transition: box-shadow 0.3s;
            width: 45em;
            height: 4em;
        }

        input[type="text"]:focus {
            box-shadow: 0 0 4px 1px rgba(55, 166, 155, 0.3);
        }

        .button {
            background-color: white;
            color: black;
            border: 2px solid #e7e7e7;
            color: black;
            padding: 12px 28px;
            margin: 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 13px;

        }

        .button:hover {
            background-color: #e7e7e7;
        }

        .shadow {
            background: #000;
            border-radius: 12px 12px 4px 4px;
            box-shadow: 0 0 20px 10px #000;
            height: 12px;
            margin: 30px auto;
            opacity: 0.2;
            width: 270px;
        }

          .Outtercenter {
                margin: auto;
                margin-top: 30px;
                width: 59%;
                height: 90%;
                text-align: center;
                padding: 10px;
                -moz-border-radius: 30px;
                -webkit-border-radius: 30px;
                border-radius: 10px;
                background-color: rgba(255, 255, 255, 0.5);
            }

        .center {
            margin: auto;
            width: 50%;

        }

        h1 {
            text-align: center;
            color: #797979;
            padding-top:45px;
        }

        label {
            font-size: 18px;
            color: #797979;

        }

        body {
            background-image: url('https://upload.wikimedia.org/wikipedia/commons/d/d0/Elegant_Background-3.jpg');
        }

        div.login {
            margin: auto;
            width: 50%;
        }

        input[type="submit"]:active {
            top: 3px;
            box-shadow: inset 0px 1px 0px #2ab7ec, 0px 2px 0px 0px #31524d, 0px 5px 3px #999;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #963D3D;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 39px;
            background-color: #333;
            color: #ffffff;
            text-align: center;
            font-size: 15px;
        }
    </style>
</head>
<body>
<!--navigation bar -->
<div class="Outtercenter">
<ul>
    <li>
        <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/user/feed">MY FEED</a>
    </li>
    <li>
        <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/search">SEARCH</a>
    </li>
    <li>
        <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/message">POST A
            MESSAGE</a>
    </li>
    <li style="float: right;" method="POST" name="logout">
        <a class="active" href="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/user/logout">LOGOUT</a>
    </li>
</ul>
    <!-- a simple search form with a button and an input box -->
    <h1 class="center">Search Messages!</h1>
    <form class="center" action="http://raptor.kent.ac.uk/proj/co539c/microblog/ns576/index.php/search/dosearch"
          method="GET">
        <input type="text" placeholder="Search here...." name="search" required><br>
        <button class="button">Search</button>
    </form>
<div class="footer"><p>(C) Nun Srijae | 2017 </p></div>
</div>
</body>
</html>

