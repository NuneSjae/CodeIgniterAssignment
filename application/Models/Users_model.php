<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //Returns TRUE if a user exists in the database and FALSE if not
    public function checkLogin($username, $pass)
    {
        $sql = "SELECT * from Users WHERE username = ? AND password = ?";
        //$query = $this->db->get('Users');
        $query = $this->db->query($sql, array($username, $pass));

        //if the query above matches with 1 row meaning there is a match in the database
        if ($query->num_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    //returns true if the user is following someone and false if they are not following anyone
    public function isFollowing($follower, $followed)
    {

        $sql = "SELECT * FROM User_Follows WHERE follower_username = ? AND followed_username = ?";
        $query = $this->db->query($sql, array($follower, $followed));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function follow($followed)
    {
        //Inserts a row into the Following table indicating that the logged-in user follows $followed
        $follower = $this->session->userdata('username');
        $sql = "INSERT INTO User_Follows (follower_username, followed_username) VALUES (?, ?)";
        $this->db->query($sql, array($follower, $followed));
    }
}
