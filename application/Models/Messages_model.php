<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages_model extends CI_Model
{
    public function __construct()
    {
        //load database into the model
        parent::__construct();
        $this->load->database();
    }
//Returns all message posted by the user with the specified username
// most recent first
    public function getMessagesByPoster($name)
    {
        $sql = "SELECT user_username, text, posted_at, id FROM Messages WHERE user_username = ? ORDER BY posted_at DESC";
        $query = $this->db->query($sql, $name);
        return $query->result();
    }
// Returns all messages containing the specified search string
    //with most recent first
    public function searchMessages($string)
    {
        $sql = "SELECT user_username, text, id, posted_at FROM Messages WHERE text LIKE '%$string%' ORDER BY posted_at DESC";
        $query = $this->db->query($sql, $string);
        return $query->result();
    }

    //adding new post into the table
    public function insertMessage($poster, $string)
    {
        $sql = "INSERT INTO Messages (user_username, text, posted_at) VALUES (?, ?, ?)";
        $this->db->query($sql, array($poster, $string, date('Y-m-d H:i:s')));


    }

    //returns all of the messages from all of those followed by the specified user
    public function getFollowedMessages($name)
    {
        $sql = "SELECT * FROM User_Follows JOIN Messages ON followed_username = user_username WHERE follower_username = ? ORDER BY posted_at DESC";
        $query = $this->db->query($sql, $name);
        return $query->result();
    }

} 




